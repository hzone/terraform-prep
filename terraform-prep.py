import os

# Demander le nom du projet à l'utilisateur
project_name = input("Entrez le nom du projet : ")

# Créer les dossiers et fichiers de base pour le projet
if not os.path.exists(project_name):
    os.makedirs(project_name)
    os.makedirs(os.path.join(project_name, "environments"))
    os.makedirs(os.path.join(project_name, "modules"))

with open(os.path.join(project_name, "main.tf"), "w") as f:
    f.write("# Terraform configuration for project " + project_name)

with open(os.path.join(project_name, "variables.tf"), "w") as f:
    f.write("# Terraform variables for project " + project_name)

with open(os.path.join(project_name, "outputs.tf"), "w") as f:
    f.write("# Terraform outputs for project " + project_name)

with open(os.path.join(project_name, "README.md"), "w") as f:
    f.write("#" + project_name)

with open(os.path.join(project_name, "terraform.tfvars"), "w") as f:
    f.write("# Main variables for project " + project_name)

with open(os.path.join(project_name, "environments", "dev.tfvars"), "w") as f:
    f.write("# Terraform environment variables for dev environment")

with open(os.path.join(project_name, "environments", "prod.tfvars"), "w") as f:
    f.write("# Terraform environment variables for prod environment")

print("Arborescence créée avec succès pour le projet " + project_name)
