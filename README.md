# Terraform Prep

Ce script crée un dossier pour le projet avec les sous-dossiers "environments" et "modules", ainsi que les fichiers "main.tf", "variables.tf", "dev.tfvars" et "prod.tfvars" dans les dossiers correspondants. 

Vous pouvez ajouter du contenu à ces fichiers pour personnaliser la configuration de votre projet.

Pour utiliser le script, il suffit de lancer la commande suivante : 

```bash
python3 main.py
```
